<?php
/**
 * AmoCRM class version 1.0.1 alpha
 * Created by sergeonko
 * Email: sergeonko@gmail.com
 * Date: 19.09.2019
 * Time: 11:40
 */

class amoClass {


    public $api_url;
    public $request;
    public $error;
    public $custom_fields;
    public $aContact;
    public $aDeal;


    protected $config;
    protected $login;
    protected $api_key;
    protected $accountData;

    public $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable',
           );

    /*
     *
     */
    function __construct($config)
    {
        $this->setConfig($config);
        $this->authorization();
        $this->getAccount();

    }// end func

    /*
     *
     */
    function setCustomFields(){

        $aCustom_fields = $this->accountData['_embedded']['custom_fields']['contacts'];

        foreach($aCustom_fields as $key => $aField){
            $result[$aField['code']] = $aField['id'];
        }// endfor

        $this->custom_fields =  $result;

        //print_r($result);
    }// end func

    /*
     *
     */
    function setConfig($config){

        if($this->validateConfig($config))
        {
            $this->api_url = $config['AMO_API_DOMAIN'];
            $this->login = $config['AMO_USER_LOGIN'];
            $this->api_key = $config['AMO_USER_API_KEY'];
            $this->config = $config;
        }

    }// end func

    /*
     * 
     */
    function getConfig(){
         return $this->config;
    }// end func
    
    /*
     *  Проверка полей конфига 
     */
    function validateConfig($config){

        if(!$config) {
            $this->error['code'] = 500;
            $this->error['text'] = "config file is undefined.";
            return;
        }

        if(!is_array($config)) {
            $this->error['code'] = 500;
            $this->error['text'] = "config file is undefined.";
            return;
        }

        $aMandatoryValues = array('AMO_API_DOMAIN', 'AMO_USER_LOGIN', 'AMO_USER_API_KEY');

        foreach($config as $key => $val){

            if(in_array($key, $aMandatoryValues) )
            {
                if(!trim($val)){
                    $this->error['code'] = 500;
                    $this->error['text'] = "Incorrect config file. ".$key.' value is undefined';
                    return;
                }
            }

        }// end for

        return true;
    }// end func

    /** Авторизация скрипта на amocrm.
     * @return int - Авторизован = true, иначе = false.
     */
    function authorization()
    {

        $aData['path'] = 'https://' . $this->api_url . '/private/api/auth.php?type=json';
        $aData['data']['USER_LOGIN']  = $this->login;
        $aData['data']['USER_HASH']  = $this->api_key;

        $result = $this->post($aData);

        if($this->error) return ;

        $aResponse = $result['response'];
        if (isset($aResponse['auth'])) return true;

        return;
    }// end func

    /*
     * Получение и предобработка прилетающих параметров
     */
    function getRequest(){

        if(!is_array($_POST)){
            return;
        } 

        foreach($_POST as $key => $val){
            $config = $this->getConfig();
            if(isset($config['contact'])) $code = $config['contact']['map'][$key];
            $this->request[$code] = htmlspecialchars($val, ENT_NOQUOTES, 'UTF-8');
        }

    }//end func


    /**
     * Апдейт контакта для привязки сделки к  контакту
     * @param $deals {array} - массив ID сделок
     */
    function editContact($deal_id)
    {

        $contact_id = '';
        if ( isset($this->aContact['_embedded']['items'][0]['id']) ) $contact_id =  $this->aContact['_embedded']['items'][0]['id'];

            $aData['data']['update'] = array(
            array(
                'id' => $contact_id,
                'updated_at' => time(),
                'leads_id' => $deal_id,
            )
        );

        $aData['path'] = 'https://' . $this->api_url . '/api/v2/contacts';
        $result = $this->post($aData);

        if(isset($result['_embedded']['items'][0]['id'])) {
            $this->findContact(); // обновляем контакт
        }

        return;
    }

    /**
     * Проверка контакта
     */
    function findContact()
    {

        $aData['path'] = 'https://' . $this->api_url . '/api/v2/contacts/?query=' . $this->request['EMAIL'];

        $result = $this->post($aData);

        if($this->error) {
            echo $this->error;
            return ;
        }

        if(isset($result['_embedded']['items'][0]['id'])){
            $this->aContact = $result;
        }
    }// end func

    /*
     * Получаем данные об акаунте (для опредлеения ID полей)
     */
    function getAccount()
    {
        $aData['path'] = 'https://' . $this->api_url . '/api/v2/account?with=custom_fields';

        $result = $this->post($aData);

        if($this->error) {
            echo $this->error;
            return ;
        }

        $this->accountData = $result;

         $this->setCustomFields();
    }// end func

    /*
     *  Создание Нового Контакта, Сделки, Задания
     */
    function createContactDealTask(){

        $this->findContact();
        if ( !isset($this->aContact['_embedded']['items'][0]['id']) ) {
            $this->addContact();
            $this->findContact();
        }
        // Добавление сделки
        $deals[] = $this->addDeal();

        if ($deals != null) {

                if (!empty($this->aContact['leads'])) {
                    foreach ($this->aContact['leads'] as $lead) {
                        $deals[] = $lead;
                    }
                }
                $this->editContact($deals);
                // TODO добавление задания
                // $tasks[] = $this->addTask();
        }

    }// end func

    /*
     *  Получение Email Field ID
     */
    function getContactEmailFieldId(){

        foreach($this->accountData['_embedded']['custom_fields']['contacts'] as $key => $val){

            if($val['code'] == 'EMAIL') {
                return $val['id'];
            }
        }
        return;
    }// end func

    /*
     * Получение  Phone Field Id
     */
    function getContactPhoneFieldId(){

        foreach($this->accountData['_embedded']['custom_fields']['contacts'] as $key => $val){

            if($val['code'] == 'PHONE') {
                return $val['id'];
            }
        }
        return;
    }// end func



    /*
     * Получить  Contact Tags
     */
    function getContactTags(){

        $config = $this->getConfig();
        return $config['contact']['tags'];
    }// end func


    /*
     * Получить ID ответственного за исполнение сделки
     */
    function getContactResponsible_id(){
        // TODO выбор исполнителя
        return $this->accountData['current_user'];

    }// end func

    /**
     * Добавления нового контакта.
     */
    function addContact()
    {

        $responsible_id = $this->getContactResponsible_id();
        $contact_tags = $this->getContactTags();
        $contact_phone_field_id = $this->getContactPhoneFieldId();
        $contact_email_field_id = $this->getContactEmailFieldId();

        $aData['path'] = 'https://' . $this->api_url .'/api/v2/contacts';
        $aData['data']['add'] = array(
            array(
                'name' => "{$this->request['NAME']}",
                'responsible_user_id' => $responsible_id,
                'created_by' => $responsible_id,
                'created_at' => time(),
                'tags' => $contact_tags, //Теги
                'custom_fields' => array(
                    array(
                        'id' => "{$contact_phone_field_id}",
                        'values' => array(
                            array(
                                'value' => "{$this->request['PHONE']}",
                                'enum' => "MOB"
                            )
                        )
                    ),
                    array(
                        'id' => $contact_email_field_id,
                        'values' => array(
                            array(
                                'value' => "{$this->request['EMAIL']}",
                                'enum' => "WORK"
                            )
                        )
                    ),
                ),
            )
        );

        $result = $this->post($aData);

        if(isset($result['_embedded']['items'][0]['id'])) {
            $this->findContact();
        }
        return;
    }


    /*
     * получить Название сделки
     */
    function getDealName(){

        $config = $this->getConfig();
        return $config['deal']['name'];

    }// end func

    /*
     *
     */
     function getDealStatusId(){
         // TODO сделать автоопределение ID статуса сделки
         return 29769250;

     }// end func

    /*
     *
     */
    function getDealAmount(){

        $config = $this->getConfig();
        return $config['deal']['amount'];

    }// end func

    /*
     *
     */
    function getDealTags(){

        $config = $this->getConfig();
        return $config['deal']['tags'];

    }// end func

    /*
     *
     */
    function getDealResponsibleId(){
        // TODO выбор ответственного
       return $this->accountData['current_user'];

    }// end func

    /*
     * Создание новой сделки
     */
    function addDeal()
    {

        $aData['data']['add'] = array(
            array(
                'name' => "{$this->getDealName()}",
                'created_at' => time(),
                'status_id' => $this->getDealStatusId(),
                'sale' => "{$this->getDealAmount()}",
                'responsible_user_id' => $this->getDealResponsibleId(),
                'tags' => "{$this->getDealTags()}"
            ),
        );

        $aData['path'] = 'https://' . $this->api_url . '/api/v2/leads';

        $result = $this->post($aData);

        if($this->error){
            echo $this->error;
            return;
        }

        if(isset($result['_embedded']['items'][0]['id']))
        $Response = $result['_embedded']['items'][0]['id'];

        return $Response;
    }// end func


    /*
    * Создание новой задачи
    */
    function addTask()
    {
        $leads['data']['add'] = array(
            array(
                'element_id' => $this->aContact['_embedded']['items'][0]['id'],
                'element_type' => "1",
                'complete_till_at' => time() + 86400 ,
                'task_type' => "1",
                'text' => "{$this->getTaskName()}",
                'created_at' => time(),
                'updated_at' => time(),
                'responsible_user_id' => "{$this->getContactResponsible_id()}",
                'created_by' => $this->accountData['current_user']
            )
        );

        $aData['path'] = 'https://' . $this->api_url . '/api/v2/leads';

        $result = $this->post($aData);

        if(isset($result['_embedded']['items'][0]['id'])) {
            $this->findContact();
        }

        return;
    }
    
    /*
     * Отправка запроса на amocrm.ru
     */
    function post($aData)
    {

        $url = $aData['path'];
        $fields = '';
        if(isset($aData['data'])) $fields = $aData['data'];

        $curl = curl_init(); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $url);
        if($fields) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($fields));
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);

        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $code = (int)$code;

        try {
                if ($code != 200 && $code != 204) {
                    $this->error = isset($this->errors[$code]) ? $this->errors[$code] : 'Undescribed error ['.$code."]";
                }
            } catch (Exception $E) {
                $this->error ='Error: ' . $E->getMessage() . ' [' . $E->getCode()."]";
            }

           $result = json_decode($out, true);

        return $result;
    }// end func

} // end class