<?php
include "config.php";
include "amoClass.php";

$oAmo = new amoClass($config);
if( $oAmo->error ) die($oAmo->error);

$oAmo->getRequest();

$oAmo->createContactDealTask();

header('Location: index.html');
