<?php
/**
 * config file for AmoCRM API access
 * https://amocrm.ru
 * Created by sergeonko
 * Date: 19.09.2019
 * Time: 11:58
 */

$config = array(
    'AMO_API_DOMAIN' => 'sergeonko.amocrm.ru', // домен amoCMS API
    'AMO_USER_LOGIN' => '***@gmail.com', // электронная почта из профиля amoCMS акаунта
    'AMO_USER_API_KEY' => '***', // Ключ доступа к API (из профиля amoCMS-акаунта)
    'contact' => array('map' => array('userName' => 'NAME',
                                      'userPhone' => 'PHONE',
                                      'userEmail' => 'EMAIL'
                                      ),
                       'tags' => 'test'
                      ),
    'deal' => array(
                    'name' => 'Заявка с сайта', //Название создаваемой сделки,
                    'amount' => 1, // сумма сделки по умолчанию
                    'tags' => 'test'
                    ),
    'task' => array(
                    'name' => 'Перезвонить клиенту',//Название задания,
                    'tags' => 'test'
                    )
    ); // custom fields map ( request name => custom field code)

